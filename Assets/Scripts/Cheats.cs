﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Cheats", order = 1)]
public class Cheats : ScriptableObject
{
    public string GunId;
    public int Money;

    public void AddPlayerMoney()
    {
        Player.Instance.SetGemsCount(Money);
    }

    public void AddGunToPlayer()
    {
        DataProvider.Instance.Gun.AddNewGun(GunId);
        
    }
    public void RemoveGunFromPlayer()
    {
        DataProvider.Instance.Gun.RemoveGun(GunId);

    }

    public void ReloadBoost()
    {
        Player.Instance.SetPowerBonus(1000, BoxType.Standart);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Cheats))]
class CheatsEditor : Editor
{


    Cheats cheats;

    void OnEnable()
    {
        cheats = (Cheats)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("AddGunToPlayer"))
        {
            cheats.AddGunToPlayer();
        }
        if (GUILayout.Button("RemoveGunFromPlayer"))
        {
            cheats.RemoveGunFromPlayer();
        }
        if (GUILayout.Button("SetMoney"))
        {
            cheats.AddPlayerMoney();
        }
        if (GUILayout.Button("ReloadBoost"))
        {
            cheats.ReloadBoost();
        }
    }
}
#endif