﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/BoxSettings", order = 1)]
public class BoxSettingsData : ScriptableObject
{
    public List<BoxSettings> BoxSettings = new List<BoxSettings>();
    public float BlastRadius;


    public BoxSettings GetSettings(int id)
    {
        if(BoxSettings.Count < id)
        {
            return BoxSettings[0];
        }
        return BoxSettings[id];
    }
}

public class BoxSettings
{
    public float Mass;
    public GameObject Prefab;
}
