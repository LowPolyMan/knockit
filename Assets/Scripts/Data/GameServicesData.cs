﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameServicesData", order = 1)]
public class GameServicesData : ScriptableObject
{
    public string GooglePlayID;
}
