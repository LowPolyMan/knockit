﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GunsData", order = 1)]
public class GunsData : ScriptableObject
{
    public List<GunData> GunDatas = new List<GunData>();
    public List<string> PlayerGuns = new List<string>();


    public string CreateFirsGun()
    {
        PlayerGuns.Clear();
        PlayerGuns.Add(GunDatas[0].GunId);
        GunList gunList = new GunList(PlayerGuns);
        var j = JsonUtility.ToJson(gunList);
        return j;
    }

    public string AddNewGunToPlayer(string id)
    {
        List<string> a = DataProvider.Instance.Gun.LoadPlayersGun();
        if(!a.Contains(id))
        {
            a.Add(id);
        }
        PlayerGuns.Clear();
        PlayerGuns.AddRange(a);

        GunList gunList = new GunList(PlayerGuns);
        var j = JsonUtility.ToJson(gunList);

        return j;
    }

    public List<string> GetPlayerGunData(string json)
    {
        GunList gunList = JsonUtility.FromJson<GunList>(json);
        return gunList.PlayerGuns;
    }

    public string RemoveGunFromPlayer(string id)
    {
        List<string> a = DataProvider.Instance.Gun.LoadPlayersGun();

        PlayerGuns.Clear();

        a.ForEach(x => { 
            if(x != id)
            {
                PlayerGuns.Add(x);
            }
        });
        
        GunList gunList = new GunList(PlayerGuns);
        var j = JsonUtility.ToJson(gunList);

        return j;
    }
}


[System.Serializable]
public class GunData
{
    public GameObject gunPrefab;
    public string GunId;
    public Sprite GunSprite;
    public int GunCost;
}

[System.Serializable]
public class GunList
{
    public List<string> PlayerGuns = new List<string>();

    public GunList(List<string> playerGuns)
    {
        PlayerGuns = playerGuns;
    }
}



