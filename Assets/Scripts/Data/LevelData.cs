﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class LevelData : MonoBehaviour
{
    public GameObject prefab;
    public List<Box> boxDatas = new List<Box>();
    public int BulletsToLevel;
    public int BoxCount;
    public bool isCustomBulletsCount = false;

    public LevelData(List<Box> boxDatas)
    {
        this.boxDatas = boxDatas;
    }

    private void Awake()
    {
        CheckBoxes();
    }

    public LevelData CheckBoxes()
    {
        prefab = gameObject;

        boxDatas.Clear();
        List<Box> boxes = new List<Box>();
        
        for(int i = 0; i < transform.childCount; i++)
        {
            var b = transform.GetChild(i).GetComponent<Box>();

            if (b == null) 
                continue;

            if (!b.GetBox() || b.GetBox().Type == BoxType.Blast || b.GetBox().Type == BoxType.Crack) 
                continue;

            boxes.Add(b);
        }

        boxDatas.AddRange(boxes);

        BoxCount = boxDatas.Count;

        if (!isCustomBulletsCount)
        {

           BulletsToLevel = 5;

        }

        return this;
    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(LevelData))]
class LevelDataEditor : Editor
{


    LevelData levelSettings;

    void OnEnable()
    {
        levelSettings = (LevelData)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Check"))
        {
            levelSettings.CheckBoxes();
        }
    }
}
#endif
