﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataProvider : MonoBehaviour
{
    public static DataProvider Instance;
    public LevelSettings LevelSettings;
    public BoxSettingsData BoxSettings;
    public Music Music;
    public GunsData GunsData;
    public Gun Gun;
    public Shop Shop;
    public List<string> PlayersGun = new List<string>();
    public GameServicesData GameServicesData;
    public Services Services;
    public UI UI;

    private void Awake()
    {
        Services = new Services();
        Services.StartUnityADS(GameServicesData.GooglePlayID);

        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance == this)
        {
            Destroy(gameObject);
        }

        if (!PlayerPrefs.HasKey("isMusicOn"))
        {
            CreatePlayerPrefs();
        }
        else
        {
            print(SaveLoad.LoadPlayerGuns());
            PlayersGun.AddRange(Gun.LoadPlayersGun());
        }
    }

    private void CreatePlayerPrefs()
    {
        print("CreateParams");
        SaveLoad.SavePlayerSettings(true, false);
        SaveLoad.SaveLevelNumber(1);
        SaveLoad.SavePlayerXP(500);
        SaveLoad.SaveBonusPower(0);
        SaveLoad.SavePlayerGems(0);
        SaveLoad.SavePlayerLevelWinCount(1);
        SaveLoad.SavePlayerGuns(GunsData.CreateFirsGun());
        SaveLoad.SaveCurrentGun("BaseGun");
    } 


}


public static class SaveLoad
{

    #region SaveData
    public static void SavePlayerXP(int value)
    {
        SaveToPlayerPrefs("playerXP", value);
    }
    public static void SavePlayerSettings(bool isMusicOn, bool isAdsOn)
    {
        SaveToPlayerPrefs("isMusicOn", isMusicOn);
        SaveToPlayerPrefs("isADSOff", isAdsOn);
    }
    public static void SaveLevelNumber(int value)
    {
        SaveToPlayerPrefs("currentLevelNumber", value);
    }
    public static void SaveBonusPower(int value)
    {
        SaveToPlayerPrefs("playerPowerBonus", value);
    }
    public static void SavePlayerGems(int value)
    {
        SaveToPlayerPrefs("playerGemsCount", value);
    }
    public static void SavePlayerLevelWinCount(int value)
    {
        SaveToPlayerPrefs("levelQ", value);
    }
    public static void SavePlayerGuns(string jsonGuns)
    {
        SaveToPlayerPrefs("playerGuns", jsonGuns);
    }
    public static void SaveCurrentGun(string gunId)
    {
        SaveToPlayerPrefs("currentGun", gunId);
    }
    #endregion
    #region LoadData
    public static int LoadPlayerXP()
    {
        return LoadIntFromPlayerPrefs("playerXP");
    }
    public static bool LoadPlayerMusicOn()
    {
        return LoadBoolFromPlayerPrefs("isMusicOn");
    }
    public static bool LoadPlayerADSOn()
    {
        return LoadBoolFromPlayerPrefs("isADSOff");
    }
    public static int LoadLevelNumber()
    {
       return LoadIntFromPlayerPrefs("currentLevelNumber");
    }
    public static int LoadBonusPower()
    {
       return LoadIntFromPlayerPrefs("playerPowerBonus");
    }
    public static int LoadPlayerGems()
    {
       return LoadIntFromPlayerPrefs("playerGemsCount");
    }
    public static int LoadPlayerLevelWinCount()
    {
       return LoadIntFromPlayerPrefs("levelQ");
    }
    public static string LoadPlayerGuns()
    {
       return LoadStringFromPlayerPrefs("playerGuns");
    }
    public static string LoadCurrentPlayerGun()
    {
        return LoadStringFromPlayerPrefs("currentGun");
    }
    #endregion
    #region SaveLoadPrivateMethods

    private static void SaveToPlayerPrefs(string paramName, bool value)
    {
        PlayerPrefs.SetInt(paramName, value ? 1 : 0);
        PlayerPrefs.Save();
    }
    private static void SaveToPlayerPrefs(string paramName, float value)
    {
        PlayerPrefs.SetFloat(paramName, value);
        PlayerPrefs.Save();
    }
    private static void SaveToPlayerPrefs(string paramName, int value)
    {
        PlayerPrefs.SetInt(paramName, value);
        PlayerPrefs.Save();
    }
    private static void SaveToPlayerPrefs(string paramName, string value)
    {
        PlayerPrefs.SetString(paramName, value);
        PlayerPrefs.Save();
    }

    private static string LoadStringFromPlayerPrefs(string paramName)
    {
        string i = PlayerPrefs.GetString(paramName);
        return i;
    }

    private static bool LoadBoolFromPlayerPrefs(string paramName)
    {
        int i = PlayerPrefs.GetInt(paramName);
        if (i == 1)
            return true;
        else
            return false;
    }
    private static float LoadFloatFromPlayerPrefs(string paramName)
    {
        float i = PlayerPrefs.GetFloat(paramName);
        return i;
    }
    private static int LoadIntFromPlayerPrefs(string paramName)
    {
        int i = PlayerPrefs.GetInt(paramName);
        return i;
    }

    #endregion

}