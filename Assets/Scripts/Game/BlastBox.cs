﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastBox : Box, IBox
{
    [SerializeField] private float radiusBlast;
    public override void Hit()
    {
        Blast();
    }

    private void Update()
    {

        if (Vector3.Distance(Startposition, transform.position) > 2f)
        {
            Blast();
        }
    
    }

    private void Blast()
    {
        deatachedTransforms.ForEach(x => { x.SetParent(transform.root); });
        BoxHitEvent.Invoke();
        Renderer.enabled = false;
        radiusBlast = DataProvider.BoxSettings.BlastRadius;
        transform.localScale = new Vector3(radiusBlast, radiusBlast, radiusBlast);
        Player.Instance.PlayerLevelChecker(2);
        Destroy(gameObject, 0.1f);
    }
}
