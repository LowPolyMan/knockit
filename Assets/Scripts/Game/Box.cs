﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public abstract class Box : MonoBehaviour, IBox
{
    [SerializeField] private BoxType type;
    [SerializeField] private bool isFixedPosition;
    [SerializeField] private UnityEvent boxHitEvent;

    private DataProvider dataProvider;
    private Rigidbody rigidbody;
    private Renderer renderer;
    private Vector3 startposition;
    private GameEvents gameEvents;

    public List<Transform> deatachedTransforms = new List<Transform>();

    public int id;

    public BoxType Type { get => Type1; private set => Type1 = value; }
    public BoxType Type1 { get => type; private set => type = value; }
    public bool IsFixedPosition { get => isFixedPosition; private set => isFixedPosition = value; }
    public UnityEvent BoxHitEvent { get => boxHitEvent; private set => boxHitEvent = value; }
    public DataProvider DataProvider { get => dataProvider; private set => dataProvider = value; }
    public Rigidbody Rigidbody { get => rigidbody; private set => rigidbody = value; }
    public Renderer Renderer { get => renderer; private set => renderer = value; }
    public Vector3 Startposition { get => startposition; private set => startposition = value; }

    private void Start()
    {
        gameEvents = GameEvents.Instance;
        Startposition = transform.position;
        Rigidbody = gameObject.GetComponent<Rigidbody>();
        DataProvider = DataProvider.Instance;
        id = (Random.Range(1, 9999999));
        gameEvents.OnBoxDownEvent += IsDown;
        gameEvents.OnBoxHit += Hit;
        Renderer = gameObject.GetComponent<Renderer>();

        if(IsFixedPosition)
        {
            Rigidbody.isKinematic = true;
        }
    }

    public BoxType? IsDown(int id, BoxType type) //call if box down on trigger ground
    {

        if (id == this.id)
        {
            AddPlayerXp();
            gameEvents.OnBoxDownEvent -= IsDown;
            gameEvents.UiUpdate();
            return type;
        }

        return null;
    }

    private void AddPlayerXp()
    {
        switch (type)
        {
            case BoxType.Standart:
                Player.Instance.PlayerLevelChecker(5);
                DataProvider.Instance.UI.CreateXpValuePrefab(5, transform.position);
                break;
            case BoxType.Havy:
                Player.Instance.PlayerLevelChecker(10);
                DataProvider.Instance.UI.CreateXpValuePrefab(10, transform.position);
                break;
            case BoxType.Blast:
                Player.Instance.PlayerLevelChecker(2);
                DataProvider.Instance.UI.CreateXpValuePrefab(2, transform.position);
                break;
            case BoxType.Crack:
                Player.Instance.PlayerLevelChecker(2);
                DataProvider.Instance.UI.CreateXpValuePrefab(2, transform.position);
                break;
        }
    }

    public void DoEnteractive()
    {
        Rigidbody.isKinematic = false;
    }

    public Box Hit(Box box)
    {
        if (box != this)
            return null;
        
        Hit();
        
        return box;
    }

    public virtual void Hit()
    {
        
    }

    public virtual Box GetBox()
    {
        return this;
    }
}


public enum BoxType
{
    Standart,
    Havy,
    Blast,
    Crack
}
