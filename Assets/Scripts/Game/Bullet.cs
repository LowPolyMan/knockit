﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : MonoBehaviour
{


    [SerializeField] private UnityEvent collisionEvent;
    private Rigidbody myRigidbody;
    bool isCollide = false;

    private void Start()
    {
        myRigidbody = gameObject.GetComponent<Rigidbody>();
        Destroy(this.gameObject, 2f);
    }

    private void OnCollisionEnter(Collision collision) //if bullet collid to any obstacles
    {
        collisionEvent.Invoke();
        myRigidbody.useGravity = true;

        if (!isCollide)
        {
            var col = collision.gameObject.GetComponent<Box>();

            if (col != null)
            {
                isCollide = true;
                col.DoEnteractive();
                GameEvents.Instance.BoxHit(col);

            }
        }
    }
}
