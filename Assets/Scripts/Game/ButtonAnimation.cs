﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAnimation : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private string proprtyName;
    [SerializeField] private Image fillImage;
    [SerializeField] private Button button;
    [SerializeField] private float time;
    private bool isActivate = false;
    [SerializeField] private Text timerText;
    [SerializeField] private GameObject star;
    [SerializeField] private GameObject video;
    [SerializeField] private GameObject powerRevardPanel;
    private void Start()
    {
        time = Player.Instance.PlayerProgression.BonusTime;
        timerText.gameObject.SetActive(false);
        star.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (!isActivate)
            return;

        time -= Time.deltaTime;
        timerText.text = time.ToString("0");
    }

    public void StartADS()
    {
        powerRevardPanel.SetActive(true);
    }


    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(time);
        isActivate = false;
        time = Player.Instance.PlayerProgression.BonusTime;
        star.SetActive(true);
        timerText.gameObject.SetActive(false);
    } 

    public void SetFillAmount(float from, float to)
    {
        if (!fillImage || isActivate)
            return;

        fillImage.fillAmount = to / from;

        if(fillImage.fillAmount < 1)
        {
            Stop();
            video.gameObject.SetActive(true);
            star.gameObject.SetActive(false);
            button.interactable = false;
        }
        else
        {
            Play();
            star.gameObject.SetActive(true);
            video.gameObject.SetActive(false);
            button.interactable = true;
        }
    }

    public void ButtonClick()
    {
        Stop();
        isActivate = true;
        timerText.gameObject.SetActive(true);
        star.SetActive(false);
        StartCoroutine(Timer());
        button.interactable = false;
        fillImage.fillAmount = 0;
        GameEvents.Instance.BonusUse();

        AnalyticsEvent.SendCustomEvent("bonus_button_click");
    }

    [ContextMenu("P")]
    public void Play()
    {
        animator.SetBool(proprtyName, true);
    }

    [ContextMenu("S")]
    public void Stop()
    {
        animator.SetBool(proprtyName, false);
    }
}
