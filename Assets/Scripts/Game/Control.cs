﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Gun))]
public class Control : MonoBehaviour
{
    private Gun _gun;
    [SerializeField] private Camera _cam;

    [SerializeField] private GraphicRaycaster m_Raycaster;
    [SerializeField] private PointerEventData m_PointerEventData;
    [SerializeField] private EventSystem m_EventSystem;

    private void Start()
    {
        _gun = gameObject.GetComponent<Gun>();
    }

    void Update()
    {  
        if (Input.GetMouseButtonDown(0))
        {
            //Set up the new Pointer Event
            m_PointerEventData = new PointerEventData(m_EventSystem);
            //Set the Pointer Event Position to that of the mouse position
            m_PointerEventData.position = Input.mousePosition;

            //Create a list of Raycast Results
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            m_Raycaster.Raycast(m_PointerEventData, results);

            if (results.Count > 0) 
                return;

            _gun.Shoot(GetDirection(_cam, Input.mousePosition));    
        }
    }


    public Vector3 GetDirection(Camera cam, Vector3 cursorPos)
    {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            return hit.point;
        }
        return new Vector3(0, 0, 0);

    }


}
