﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackBox : Box, IBox
{
    public override void Hit()
    {
        Crack();
    }

    private void Crack()
    {
        deatachedTransforms.ForEach(x => { x.SetParent(transform.root); });
        BoxHitEvent.Invoke();

        gameObject.SetActive(false);
        Player.Instance.PlayerLevelChecker(2);

    }

    private void Update()
    {
        if (Type1 == BoxType.Crack)
        {
            if (Vector3.Distance(Startposition, transform.position) > 1f)
            {
                Crack();
            }
        }
    }
}
