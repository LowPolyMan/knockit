﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyChecker : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var b = other.gameObject.GetComponent<Box>();

        if (b != null)
        {
            GameEvents.Instance.BoxDown(other.gameObject.GetComponent<Box>().id, b.Type);
            Destroy(b.gameObject, 0.5f);
            return;
        }

        var c = other.gameObject.GetComponent<Bullet>();

        if (c != null)
        {
            Destroy(other.gameObject);
        }
    }
}
