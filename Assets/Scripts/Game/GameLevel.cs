﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class GameLevel : MonoBehaviour
{
    public static GameLevel Instance; 

    [SerializeField] private int currentlevelNumber; 
    [SerializeField] private LevelData currentLevelData;
    public int LevelNumber => currentlevelNumber;
    [SerializeField] private int boxDownCount = 0;
    [SerializeField] private int boxTowinCount = 0;
    private GameEvents gameEvents;
    private int loseLevelCount = 0;
    private int winLevelCount = 0;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        gameEvents = GameEvents.Instance;
        currentlevelNumber = SaveLoad.LoadLevelNumber();
        StartCoroutine(SlowStart());
        gameEvents.OnLevelFail += LevelFail;
        gameEvents.OnBoxDownEvent += AddScore;
        gameEvents.OnLevelSkipEvent += SkipLevel;
        
    }

    private IEnumerator SlowStart()
    {
        yield return new WaitForSeconds(0.1f);
        SelectLevel();
    }

    public void NextLevel()
    {
        if (DataProvider.Instance.LevelSettings.levelDatas.ToArray().Length >= currentlevelNumber)
        {
            StartLevel(currentlevelNumber);
        }
    }

    public void RestartLevel()
    {
        StartLevel(currentlevelNumber);
    }

    private void SelectLevel()
    {
        if(currentlevelNumber <= DataProvider.Instance.LevelSettings.levelDatas.Count)
        {
            StartLevel(currentlevelNumber);
        }
        else
        {
            int minLevel = 2;
            int maxLevel = DataProvider.Instance.LevelSettings.levelDatas.Count;
            StartLevel(Random.Range(minLevel, maxLevel + 1));
        }
    }

    public void StartLevel(int levelNumber)
    {
        RemoveLevel();
        boxDownCount = 0;

        Player.Instance.SetPlayerWinStatus(false);
        currentlevelNumber = levelNumber;
        LevelData data = DataProvider.Instance.LevelSettings.levelDatas.ToArray()[levelNumber - 1];

        currentLevelData = Instantiate(data.CheckBoxes()); //load new level
        Player.Instance.AddBullet(DataProvider.Instance.LevelSettings.levelDatas.ToArray()[levelNumber - 1].BulletsToLevel); //add bullets to player
        boxTowinCount = currentLevelData.BoxCount;
        gameEvents.LevelStart();
    }

    public void LevelWin()
    {
        Player.Instance.PlayerLevelChecker(currentLevelData.BoxCount * 10);
        Player.Instance.SetPlayerWinStatus(true);
        currentlevelNumber++;
        Player.Instance.GetSetLevelQ(1);
        SaveLoad.SaveLevelNumber(currentlevelNumber);
        gameEvents.LevelWin();
        winLevelCount++;

        AnalyticsEvent.SendCustomEvent("level_win", new Dictionary<string, object>
        {
            {"level_number", currentlevelNumber - 1},
            {"level_q", Player.Instance.GetSetLevelQ()}
        });

        if(winLevelCount >= Player.Instance.PlayerProgression.WinLevelCountToADS)
        {
            DataProvider.Instance.Services.StartVideo();
            winLevelCount = 0;
        }

        SelectLevel();
    }

    public void LevelFail()
    {
        Player.Instance.PlayerLoose = true;
        RemoveLevel();
        StartLevel(currentlevelNumber);
        loseLevelCount++;
        if (loseLevelCount >= Player.Instance.PlayerProgression.LoseLevelCountToADS)
        {
            //skip level ADS
            DataProvider.Instance.UI.ShowLevelFailPanel();
            loseLevelCount = 0;
        }

        AnalyticsEvent.SendCustomEvent("level_losse", new Dictionary<string, object>
        {
            {"level_number", currentlevelNumber},
            {"losse_level_q", loseLevelCount},
            {"level_q", Player.Instance.GetSetLevelQ()}
        });
    }

    public void SkipLevel()
    {
        Player.Instance.SetPlayerWinStatus(true);
        currentlevelNumber++;
        Player.Instance.GetSetLevelQ(1);
        SaveLoad.SaveLevelNumber(currentlevelNumber);
        gameEvents.LevelWin();
        SelectLevel();

        AnalyticsEvent.SendCustomEvent("level_skip", new Dictionary<string, object>
        {
            {"level_number", currentlevelNumber - 1}
        });
    }

    private BoxType? AddScore(int score, BoxType type)
    {
        switch (type)
        {
            case BoxType.Standart:
                Score();
                break;
            case BoxType.Havy:
                Score();
                break;
        }     

        return type;
    }

    public void Score()
    {
        boxDownCount++;
        if (boxDownCount >= boxTowinCount)
        {
            LevelWin();
        }
    }

    public void RemoveLevel()
    {
        if (!currentLevelData) return;

        Destroy(currentLevelData.gameObject);
    }

  

    [System.Serializable]
    public class Level
    {
        public int CubCount;
        public int Score;
        public GameObject LevelObject;
    }

    private void OnDisable()
    {
        gameEvents.OnLevelFail -= LevelFail;
        gameEvents.OnBoxDownEvent -= AddScore;
        gameEvents.OnLevelSkipEvent -= SkipLevel;
    }

    public void SelectCurrentLevel()
    {
        PlayerPrefs.SetInt("currentLevelNumber", currentlevelNumber);
        PlayerPrefs.Save();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GameLevel))]
class GameLevelEditor : Editor
{


    GameLevel gamelevel;

    void OnEnable()
    {
        gamelevel = (GameLevel)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("SelectCurrentLevel"))
        {
            gamelevel.SelectCurrentLevel();
        }
    }
}
#endif
