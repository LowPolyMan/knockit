﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gun : MonoBehaviour
{
    public GunSettings CurrentGunSettings;
    public List<GameObject> Guns = new List<GameObject>();

    [SerializeField] private Transform _barrel;
    [SerializeField] private Transform _gun;
    [SerializeField] private float _gunPower; 
    [SerializeField] private float _cooldown;
    public bool isBonused = false;
    private Vector3 _startBulletPoint;
    private Vector3 shootPoint = new Vector3(0,0,0);
    private Vector3 oldShootPoint;
    private void Start()
    {
        _cooldown = Player.Instance.PlayerProgression.GunCooldownt;
        _startBulletPoint = _barrel.transform.position;
        GameEvents.Instance.OnPowerBonusUse += ActivateBonusBoost;

        SelectGun(DataProvider.Instance.Gun.GetCurrentGun());

        shootPoint = _barrel.forward * 50f;
        oldShootPoint = shootPoint;
    }

    private void Update()
    {
        if(_cooldown > 0)
            _cooldown -= Time.deltaTime;

        if (isBonused && _cooldown <= 0)
            Shoot(shootPoint);
        
    }

    public void SetCurrentGun(string id)
    {
        SaveLoad.SaveCurrentGun(id);
        Player.Instance.CurrentPlayerGunId = id;
    }
    public string GetCurrentGun()
    {
        return SaveLoad.LoadCurrentPlayerGun();
    }

    public List<string> LoadPlayersGun()
    {
        return DataProvider.Instance.GunsData.GetPlayerGunData(SaveLoad.LoadPlayerGuns());
    }

    public void AddNewGun(string id)
    {
        SaveLoad.SavePlayerGuns(DataProvider.Instance.GunsData.AddNewGunToPlayer(id));
        print("New Gun Added:  " + SaveLoad.LoadPlayerGuns());
    }

    public void RemoveGun(string id)
    {
        SaveLoad.SavePlayerGuns(DataProvider.Instance.GunsData.RemoveGunFromPlayer(id));
        print("Gun:  " + id + " remove:  " + SaveLoad.LoadPlayerGuns());
    }
    public void SelectGun(string id)
    {
        Guns.ForEach(x => { x.SetActive(false); });
        Guns.ForEach(x =>
        {
            if(x.GetComponent<GunSettings>().Id == id)
            {
                x.SetActive(true);
                CurrentGunSettings = x.GetComponent<GunSettings>();
                print("gun selection: " + id);
                return;
            }
        });

        SetCurrentGun(id);
    }

    public void ActivateBonusBoost()
    {
        StartCoroutine(BonusTime(Player.Instance.PlayerProgression.BonusTime));
    }

    private IEnumerator BonusTime(float time)
    {
        isBonused = true;
        _cooldown = Player.Instance.PlayerProgression.GunBonusCooldount;
        yield return new WaitForSeconds(time);
        _cooldown = Player.Instance.PlayerProgression.GunCooldownt;
        isBonused = false;

    }

    public void Shoot(Vector3 point)
    {
        shootPoint = point;

        if (_cooldown > 0 || Player.Instance.GetPlayerWinStatus() == true || Player.Instance.PlayerLoose == true || Player.Instance.BulletCount < 1) return;

        GameObject bullet = Instantiate(CurrentGunSettings.BulletPrefab, _startBulletPoint, Quaternion.identity);
        Transform _bulletTransform = bullet.transform;

    
        _bulletTransform.LookAt(point);
        bullet.GetComponent<Rigidbody>().AddForce(_bulletTransform.forward * _gunPower);


        _gun.LookAt(point);


        CurrentGunSettings.Shoot();

        if(isBonused)
            _cooldown = Player.Instance.PlayerProgression.GunBonusCooldount;
        else
        {
            Player.Instance.RemoveBullet();
            _cooldown = Player.Instance.PlayerProgression.GunCooldownt;
        }
            
    }

    private void OnDestroy()
    {
        GameEvents.Instance.OnPowerBonusUse -= ActivateBonusBoost;
    }
}
