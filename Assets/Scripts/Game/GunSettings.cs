﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GunSettings : MonoBehaviour
{
    public UnityEvent ShootEvent;
    public GameObject BulletPrefab;
    public string Id;
    public void Shoot()
    {
        ShootEvent.Invoke();
    }
}
