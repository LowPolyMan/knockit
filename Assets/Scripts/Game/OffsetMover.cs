﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetMover : MonoBehaviour
{
    public float Speed;

    private Renderer _renderer;

    private void Start()
    {
        _renderer = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float offset = Time.time * Speed;
        _renderer.material.SetTextureOffset("_BaseMap", new Vector2(offset, 0));
    }
}
