﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    
    public int PlayerLevel;
    public int CurrentPlayerXp;
    [SerializeField] private int levelQ;
    public PlayerProgressionData PlayerProgression;
    public static Player Instance;
    public string CurrentPlayerGunId;
    public bool PlayerLoose = false;
    public int BulletCount => bulletCount;

    private bool _playerWin;
    private GameEvents gameEvents;
    [SerializeField] private DataProvider dataProvider;
    [SerializeField] private float powerBouns;
    [SerializeField] private int bulletCount;
    [SerializeField] private int gemsCount;
    



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance == this)
        {
            Destroy(gameObject);
        }

    }
    private void Start()
    {
        dataProvider = DataProvider.Instance;
        gameEvents = GameEvents.Instance;

        gameEvents.OnPowerBonusUse += UsePowerBonus;
        gameEvents.OnBoxDownEvent += SetPowerBonus;

        PlayerLevelChecker(SaveLoad.LoadPlayerXP());
        powerBouns = SaveLoad.LoadBonusPower();
        gemsCount = SaveLoad.LoadPlayerGems();
        levelQ = SaveLoad.LoadPlayerLevelWinCount();
        gameEvents.UiUpdate();
       
    }
    public int GetSetLevelQ(int levelcount = 0)
    {
        levelQ += levelcount;
        if(levelQ >= PlayerProgression.RewardPeriod)
        {
            levelQ = 1;
            gemsCount += PlayerProgression.RewardValue;
            DataProvider.Instance.UI.ShowX2RewardPanel();
        }

        SaveLoad.SavePlayerLevelWinCount(levelQ);
        SaveLoad.SavePlayerGems(gemsCount);

        return levelQ;
    }
    public int GetGemsCount()
    {
        return gemsCount;
    }
    public void SetGemsCount(int value)
    {
        gemsCount += value;
        SaveLoad.SavePlayerGems(gemsCount);
        GameEvents.Instance.UiUpdate();
    }

    public float GetPowerCount()
    {
        return powerBouns;
    }

    public void UsePowerBonus()
    {
        powerBouns = 0;
        SaveLoad.SaveBonusPower((int)powerBouns);
        gameEvents.UiUpdate();
    }

    public BoxType? SetPowerBonus(int a, BoxType boxType)
    {
        if (DataProvider.Instance.Gun.isBonused) return boxType;

        powerBouns += 1;
        SaveLoad.SaveBonusPower((int)powerBouns);
        gameEvents.UiUpdate();
        return boxType;
    }

    public void SetPowerBonusReward(int value)
    {
        powerBouns = value + 1;
        SaveLoad.SaveBonusPower((int)powerBouns);
        gameEvents.UiUpdate();
    }


    public void PlayerLevelChecker(int value)
    {
        PlayerLevel = 1;
        CurrentPlayerXp += value;
        SetPlayerLevel(value);      
    }

    private void SetPlayerLevel(int xp)
    {
        SaveLoad.SavePlayerXP(CurrentPlayerXp);
        PlayerLevel = CurrentPlayerXp / (PlayerProgression.LevelDelta);
    }

    public int GetCalculatedPlayerLevel()
    {
        return SaveLoad.LoadPlayerXP() / (PlayerProgression.LevelDelta);
    }

    public void SetPlayerWinStatus(bool value)
    {
        _playerWin = value;
    }
    public bool GetPlayerWinStatus()
    {
        return _playerWin;
    }
    public void RemoveBullet()
    {
        if (bulletCount > 0)
            bulletCount--;

        gameEvents.BulletCountChange();

        if(bulletCount == 0)
             StartCoroutine(CheckAfterShoot());
    }

    private bool CheckFail(int value)
    {
        if (value == 0 && _playerWin == false)
        {
            return true;
        }
        else
            return false;
    }

    private IEnumerator CheckAfterShoot()
    {
        yield return new WaitForSeconds(3f);

        if (CheckFail(bulletCount))
        {
            PlayerLoose = true;
            gameEvents.LevelFail();
        }
    }

    public void AddBullet(int count)
    {
        PlayerLoose = false;
        bulletCount = count;
        GameEvents.Instance.BulletCountChange();   
    }


}
