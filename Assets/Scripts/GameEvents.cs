﻿using System;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents Instance;
    public Services Services;


    public event Func<int, BoxType, BoxType?> OnBoxDownEvent;
    public event Action OnBulletCountChange;
    public event Action OnLevelFail;
    public event Action OnLevelWin;
    public event Action OnLevelStart; 
    public event Action OnUiUpdate;
    public event Action OnPowerBonusUse;
    public event Action OnLevelSkipEvent;
    public event Func<Box, Box> OnBoxHit;

    private void Awake()
    {
        Instance = this;
    }

    public BoxType BoxDown(int id, BoxType type) //call if box down on trigger ground
    {
        if(OnBoxDownEvent != null)
        {
            OnBoxDownEvent(id, type);
        }

        return type;
    }

    public Box BoxHit(Box box)
    {
        if(OnBoxHit != null)
        {
            OnBoxHit(box);
            
        }
        return box;
    }

    public void LevelSkip()
    {
        if (OnLevelSkipEvent != null)
        {
            OnLevelSkipEvent();

        }
    }

    public void BulletCountChange() //call on bullet count change
    {
        if(OnBulletCountChange != null)
        {
            OnBulletCountChange();
        }
    }

    public void BonusUse() //call if bullet == 0
    {
        if (OnPowerBonusUse != null)
        {
            OnPowerBonusUse();
        }
    }

    public void LevelFail() //call if bullet == 0
    {
        if (OnLevelFail != null)
        {
            OnLevelFail();
        }
    }

    public void LevelWin() //call if all cub down on trigger grownd
    {
        if (OnLevelWin != null)
        {
            OnLevelWin();
        }
    }

    public void LevelStart() // call on level starts
    {
        if (OnLevelStart != null)
        {
            OnLevelStart();
        }
    }

    public void UiUpdate() // call if need update UI
    {
        if (OnUiUpdate != null)
        {
            OnUiUpdate();
        }
    }

}
