﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBox
{
    Box GetBox();
    Box Hit(Box box);   
}
