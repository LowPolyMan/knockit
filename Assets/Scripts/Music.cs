﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public List<AudioSource> audioClips = new List<AudioSource>();
    private bool _isPlayMusic = true;
    private bool _isPlayVfx = true;
    public bool MusicStatus;

    public void Start()
    {
        if (SaveLoad.LoadPlayerMusicOn())
        {
            audioClips[0].Play();
        }
        else
        {
            audioClips[0].Stop();
        }
        
    }

    public void MuteUnmuteMusic()
    {
        if(!_isPlayMusic)
        {
            audioClips[0].Play();
            _isPlayMusic = !_isPlayMusic;
        }
        else
        {
            audioClips[0].Stop();
            _isPlayMusic = !_isPlayMusic;
        }

        

        MusicStatus = _isPlayMusic;

        SaveLoad.SavePlayerSettings(MusicStatus, SaveLoad.LoadPlayerADSOn());
    }



}
