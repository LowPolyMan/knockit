﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PlayerProgression", order = 1)]
public class PlayerProgressionData : ScriptableObject
{
    public int LevelDelta;
    public int RewardPeriod;
    public int LevelNumberPerLevel;
    public float ProgressionMull;
    public float PowerBonusCount;
    public float GunCooldownt;
    public float BonusTime;
    public float GunBonusCooldount;
    public int RewardLevelCount;
    public int RewardValue;
    public int LoseLevelCountToADS;
    public int WinLevelCountToADS;

}
