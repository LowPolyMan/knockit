﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;

public class Services
{
    public enum RewardType
    {
        Power, LevelSkip, X2Reward, NewGun
    }

    private UnityADS UnityADS;
    private RewardType rewardType;


    public void StartUnityADS(string id)
    {

        UnityADS = new UnityADS(id);
    }

    public void StartRewardedVideo(Services.RewardType _rewardType)
    {
        rewardType = _rewardType;
        UnityADS.StartRewardedADS(_rewardType);
    }

    public void StartVideo()
    {
        UnityADS.StartVideo();
    }
}

public static class AnalyticsEvent
{
    public static void SendCustomEvent(string eventID, Dictionary<string, object> keyValuePairs)
    {
        Analytics.CustomEvent(eventID, keyValuePairs);
    }
    public static void SendCustomEvent(string eventID)
    {
        Analytics.CustomEvent(eventID);
    }
}

public class UnityADS : IUnityAdsListener
{
    private string myPlacementIdRewardedADS = "rewardedVideo";
    private Services.RewardType rewardType;

    public UnityADS(string id)
    {
        if (Advertisement.isSupported)
        {
            Advertisement.AddListener(this);
            Advertisement.Initialize(id, false);        
            Debug.Log(@"UnityAds is started!");
        }     
    }


    public void StartRewardedADS(Services.RewardType _rewardType)
    {
        rewardType = _rewardType;
        Advertisement.Show(myPlacementIdRewardedADS);
    }

    public void StartVideo()
    {
        Advertisement.Show("video");
    }

    private void RewardedPower()
    {
        Player.Instance.SetPowerBonusReward((int)Player.Instance.PlayerProgression.PowerBonusCount);
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            // Reward the user for watching the ad to completion.
            Debug.Log("REWARDED");

            switch(rewardType)
            {
                case Services.RewardType.Power:
                    RewardedPower();
                    break;
                case Services.RewardType.LevelSkip:
                    GameEvents.Instance.LevelSkip();
                    break;
                case Services.RewardType.X2Reward:
                    Player.Instance.SetGemsCount(Player.Instance.PlayerProgression.RewardValue);
                    break;
                case Services.RewardType.NewGun:
                    DataProvider.Instance.Gun.AddNewGun("Cow");
                    break;

            }
            
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
            Debug.Log("Skipped");
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
        if (placementId == myPlacementIdRewardedADS)
        {
            Debug.Log(@"ADS ready");
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }


}
