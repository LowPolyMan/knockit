﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ByPanel : MonoBehaviour
{
    [SerializeField] private Image goodImage;
    [SerializeField] private Text costText;
    [SerializeField] private ShopItem shopItem;
    [SerializeField] private Button byButton;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void OpenPanel(Sprite goodSprite, int cost, ShopItem shopitem)
    {
        goodImage.sprite = goodSprite;
        costText.text = cost.ToString();
        shopItem = shopitem;

        if(cost > Player.Instance.GetGemsCount())
        {
            byButton.interactable = false;
        }
        else
        {
            byButton.interactable = true;
        }
    }

    public void By()
    {
        shopItem.ByItem();
    }
}
