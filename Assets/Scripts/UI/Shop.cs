﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject ShopItemPrefab;
    public Transform ShopPanelTransform;
    public GunsData GunsData;
    public ByPanel ByPanel;
    public List<ShopItem> ShopItems = new List<ShopItem>();
    [SerializeField] private DataProvider dataProvider;
    private bool isShopOpen = false;

    public void OpenShop()
    {
        dataProvider = DataProvider.Instance;

        if(!isShopOpen)
        {
            CreateShopItemsList();
        }
           
        isShopOpen = !isShopOpen;

        AnalyticsEvent.SendCustomEvent("shop_open");
    }
    
    public void SelectGun(string id)
    {
        DataProvider.Instance.Gun.SelectGun(id);
        CreateShopItemsList();
    }

    public void ByItem(string id, int cost)
    {
        DataProvider.Instance.Gun.AddNewGun(id);
        Player.Instance.SetGemsCount(-cost);
        DataProvider.Instance.Gun.SelectGun(id);
        CreateShopItemsList();
        GameEvents.Instance.UiUpdate();

        AnalyticsEvent.SendCustomEvent("shop_by_item", new Dictionary<string, object>
        {
            {"item_id", id},
            {"item_cost", cost}
        });
    }

    public void CreateShopItemsList()
    {
        ShopItems.ForEach(x => {
            x.DestroySelf();
        });

        ShopItems.Clear();
        StartCoroutine(CreateShop());
    }
    private IEnumerator CreateShop()
    {
        yield return new WaitForSeconds(0.1f);

        foreach (var gun in GunsData.GunDatas)
        {

            ShopItem shopItem = Instantiate(ShopItemPrefab, ShopPanelTransform).GetComponent<ShopItem>();
            ShopItems.Add(shopItem);
            shopItem.Create(gun.GunSprite, gun.GunCost, CheckInPlayerAvalableGun(gun.GunId), gun.GunId);
        }
    }

    public bool CheckInPlayerAvalableGun(string gunId)
    {
        return dataProvider.Gun.LoadPlayersGun().Contains(gunId);

    }
}
