﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    [SerializeField] private Sprite gunSprite;
    [SerializeField] private Image gunImage;
    [SerializeField] private int gunCost;
    [SerializeField] private Text gunCostText;

    [SerializeField] private GameObject buttonBy;
    [SerializeField] private GameObject buttonSelect;
    [SerializeField] private GameObject selectPanel;
    [SerializeField] private GameObject costPanel;
    [SerializeField] private GameObject checkBox;
    public string Id;

    public void SelectGun()
    {
        DataProvider.Instance.Shop.SelectGun(Id);
    }

    public void ByItem()
    {
        DataProvider.Instance.Shop.ByItem(Id, gunCost);
    }

    public void OpenByPanel()
    {
        DataProvider.Instance.Shop.ByPanel.OpenPanel(gunSprite, gunCost, this);
        DataProvider.Instance.Shop.ByPanel.gameObject.SetActive(true);
    }

    public void Create(Sprite sprite, int cost, bool isInPlayer, string id)
    {
        Id = id;
        gunImage.sprite = sprite;
        gunSprite = sprite;
        gunCost = cost;
        gunCostText.text = cost.ToString();

        if(isInPlayer)
        {
            costPanel.SetActive(false);
            selectPanel.SetActive(true);
            buttonBy.SetActive(false);
            buttonSelect.SetActive(true);
        }
        else 
        {
            costPanel.SetActive(true);
            selectPanel.SetActive(false);
            buttonBy.SetActive(true);
            buttonSelect.SetActive(false);
        }

        if(Player.Instance.CurrentPlayerGunId == id)
        {
            checkBox.SetActive(true);
        }
        else
        {
            checkBox.SetActive(false);
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject, 0.1f);
    }

}
