﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private Text _bulletCountText;
    [SerializeField] private GameObject _winLevelPanel;
    [SerializeField] private GameObject _looseLevelPanel;
    [SerializeField] private Text _currentLevelText;
    [SerializeField] private Text _nextLevelText;
    [SerializeField] private Slider _progressBar;
    [SerializeField] private Text playerScore;
    [SerializeField] private Text gemsCountText;
    [SerializeField] private ButtonAnimation powerBonusImage;
    [SerializeField] private Toggle musicToggle;
    [SerializeField] private GameObject levelFailPanel;
    [SerializeField] private GameObject x2RewardPanel;
    [SerializeField] private GameObject xpValuePrefab;

    private Player player;
    private GameEvents gameEvents;
    private void Start()
    {
        player = Player.Instance;
        gameEvents = GameEvents.Instance;

        gameEvents.OnBulletCountChange += UpdateUI;
        gameEvents.OnLevelFail += LevelFail;
        gameEvents.OnLevelWin += LevelWin;
        gameEvents.OnLevelStart += LevelStart;
        gameEvents.OnUiUpdate += UpdateUI;
        musicToggle.isOn = SaveLoad.LoadPlayerMusicOn();
        UpdateUI();
    }

    public void UpdateUI()
    {
        _bulletCountText.text = "X " + player.BulletCount.ToString();
        _currentLevelText.text =  player.PlayerLevel.ToString();
        _nextLevelText.text =  (player.PlayerLevel + 1).ToString();
        playerScore.text = player.CurrentPlayerXp.ToString() + " / " + player.PlayerProgression.LevelDelta * (player.PlayerLevel + 1);
        gemsCountText.text = Player.Instance.GetGemsCount().ToString();
        powerBonusImage.SetFillAmount(player.PlayerProgression.PowerBonusCount, player.GetPowerCount());
        UpdateLevelSlider();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void UpdateLevelSlider()
    {
        if (player.PlayerLevel != 0)
        {
            _progressBar.maxValue = player.PlayerProgression.LevelDelta;
            _progressBar.value = player.CurrentPlayerXp - player.PlayerProgression.LevelDelta * player.PlayerLevel;
        }
        else
        {
            _progressBar.maxValue = player.PlayerProgression.LevelDelta;
            _progressBar.value = player.CurrentPlayerXp;
        }

    }

    public void ShowLevelFailPanel()
    {
        levelFailPanel.SetActive(true);
    }

    public void ShowX2RewardPanel()
    {
        x2RewardPanel.SetActive(true);
    }

    public void CreateXpValuePrefab(int value, Vector3 position)
    {
        UiXpValueObject uiXpValueObject = Instantiate(xpValuePrefab, transform).GetComponent<UiXpValueObject>();
        uiXpValueObject.gameObject.transform.localScale = new Vector3(1, 1, 1);
        uiXpValueObject.gameObject.transform.position = Camera.main.WorldToScreenPoint(position);
        uiXpValueObject.valueText.text = " + " + value.ToString();
    }

    public void ClickLevelFailRewardedVideoButton()
    {
        DataProvider.Instance.Services.StartRewardedVideo(Services.RewardType.LevelSkip);
    }
    public void ClickX2RewardedVideoButton()
    {
        DataProvider.Instance.Services.StartRewardedVideo(Services.RewardType.X2Reward);
    }

    public void ClickPowerRewardedVideoButton()
    {
        DataProvider.Instance.Services.StartRewardedVideo(Services.RewardType.Power);
    }

    private void LevelFail()
    {
        print("Level Failed!");
        // _looseLevelPanel.SetActive(true);
    }
    private void LevelWin()
    {
        print("Level Win!");
        // _winLevelPanel.SetActive(true);
    }
    private void LevelStart()
    {
        print("Level Start!");

        _winLevelPanel.SetActive(false);
        _looseLevelPanel.SetActive(false);
    }

    private void OnDisable()
    {
        gameEvents.OnBulletCountChange -= UpdateUI;
        gameEvents.OnLevelFail -= LevelFail;
        gameEvents.OnLevelWin -= LevelWin;
        gameEvents.OnLevelStart -= LevelStart;
        gameEvents.OnUiUpdate -= UpdateUI;
    } 
}
