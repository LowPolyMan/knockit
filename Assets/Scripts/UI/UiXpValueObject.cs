﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiXpValueObject : MonoBehaviour
{
    public Text valueText;
    public Color textColor;
    public float Speed;
    public float DestroyTime;

    private void Start()
    {
        Destroy(this.gameObject, DestroyTime);
    }

    private void Update()
    {
        transform.position += new Vector3(0,Time.deltaTime * Speed,0);
    }
}
